//when the js file is loaded, show list function will called to show user list.

$(window).load(function(){
    showlist("username");
});

//handle the register event:
document.querySelector('#RegisterPage button').onclick = function() {
	console.log('sign up button clicked');
	//create varibles to store information
	var username = document.querySelectorAll('#RegisterPage input')[0].value;
	var name = document.querySelectorAll('#RegisterPage input')[1].value;
	var email = document.querySelectorAll('#RegisterPage input')[2].value;
	var password = document.querySelectorAll('#RegisterPage input')[3].value;
	var age = document.querySelectorAll('#RegisterPage input')[4].value;
	// check if the input is not empty
	if(email !="" && password != "" && username !="" && name!= "" && age !=0){
		
		 createUser(username,name,email,password,age);
			

		}else{
			alert("please fill in all the textbox!");
		}
		

	

	// after create account empty the input box	
	document.querySelectorAll('#RegisterPage input')[0].value='';
	document.querySelectorAll('#RegisterPage input')[1].value='';
	document.querySelectorAll('#RegisterPage input')[2].value='';
	document.querySelectorAll('#RegisterPage input')[3].value='';
	document.querySelectorAll('#RegisterPage input')[4].value='';
	showlist("username");
	
	 	
}

//create user account with checkings
function createUser(username,name,email,password,age){
	//get user information from database
var url ="https://account-4c3f3.firebaseio.com/users/"+ username +".json"
var user=""
var jsonResult;
			$.ajax({
		     type: 'GET',
		     url: url,
		     dataType: 'json',
		     success: function(data) {
				     	//if there are return successful return
				     	if(data!=null){
				     		//store in a varible
				          user=data.username
		     	}
		       		//check if the user have a value , if not create account,
				  if(user==""){
				  			var ref = firebase.database().ref('users/' + username);
							writeUserData(username,name,email,password,age);
							alert("account created!"); }
							//else show error message
		   		  else{
					
							alert("user is already exist!");
				}
		     }
		 });
  
 
 }


//this function is for show user list. 

function showlist(order){
	//order is for checking the list should order by
	//create a table object
	var list = document.createElement('table');
	list.setAttribute("border","1");
	var th ="<th>ID</th><th>Username</th><th>Name</th><th>Age</th><th>Email</th>"
	list.innerHTML+=th;  
		// user list will sort by user age
		if(order =="age"){
					var url ="https://account-4c3f3.firebaseio.com/users.json"
					var jsonResult=[];
					$.ajax({
				     type: 'GET',
				     url: url,
				     dataType: 'json',
				     success: function(data) {
				    	$.each(data, function(index) {
				
				       jsonResult.push(data[index])
				    });
				    	
					   
					   jsonResult.sort(function(a, b){
					    var keyA = parseInt(a.age);
					        keyB = parseInt(b.age);
					   
					    if(keyA < keyB) return -1;
					    if(keyA > keyB) return 1;
					    return 0;
					});
										   
					  for (i=0;i<jsonResult.length;i++){
					  	//create table object to represent result
					  	var rw = document.createElement('tr');
						var tc ="<td>"+ jsonResult[i].id +"</td>"+
								"<td>"+ jsonResult[i].username +"</td>"+
								"<td>"+ jsonResult[i].name +"</td>"+
								"<td>"+ jsonResult[i].age +"</td>"+
								"<td>"+ jsonResult[i].email +"</td>";

						rw.innerHTML+=tc;
						list.appendChild(rw);
									  }
								     }
								     
								 });
												
		}else{
			//user list will sort by input order
			firebase.database().ref("users").orderByChild(order).on("child_added", function(snapshot) {
						//create table object to represent result
						var rw = document.createElement('tr');
						var tc ="<td>"+ snapshot.val().id +"</td>"+
								"<td>"+ snapshot.val().username +"</td>"+
								"<td>"+ snapshot.val().name +"</td>"+
								"<td>"+ snapshot.val().age +"</td>"+
								"<td>"+ snapshot.val().email +"</td>";

						rw.innerHTML+=tc;
						list.appendChild(rw);
		
			});
		}
		
			createButton(list);
	
		

		
}

//this function is for calculate average age for all the users.
function getAge(){
				var url ="https://account-4c3f3.firebaseio.com/users.json"
				var jsonResult=[];
				$.ajax({
					     type: 'GET',
					     url: url,
					     dataType: 'json',
					     success: function(data) {
					    	$.each(data, function(index) {
					
					       jsonResult.push(data[index].age)
			    });
					    	var age=0
					    	for(i=0;i<jsonResult.length;i++){
					    		age+=parseInt(jsonResult[i])
					    	}
					     	alert("the average age is "+parseFloat(age/jsonResult.length));
					     }
			     
			 });
				
			}
	
    






// handle the event sort by username
document.querySelectorAll('#condition button')[2].onclick = function() {
		showlist("username");	
}



	
	
//filter by age return user list who are greater or less than user input
function ageCondition(age,event){
		
			var url ="https://account-4c3f3.firebaseio.com/users.json"
			var jsonResult=[];
				$.ajax({
			     type: 'GET',
			     url: url,
			     dataType: 'json',
			     success: function(data) {
					    	$.each(data, function(index) {
					    	if(event=="gt"){
								if(parseInt(data[index].age) >= parseInt(age)){
					    		 jsonResult.push(data[index])
								}
					    	}else if(event=="lt"){
					    			if(data[index].age <= age){
					    		 jsonResult.push(data[index])
								}
			    	}
		    });
    				//create some element to show on the web page
		    		var list = document.createElement('table');
		    		list.setAttribute("border","1");
		    		var th ="<th>ID</th><th>Username</th><th>Name</th><th>Age</th><th>Email</th>"
					list.innerHTML+=th;    	
    				for(i=0;i<jsonResult.length;i++){

						var rw = document.createElement('tr');
				
						var tc ="<td>"+ jsonResult[i].id +"</td>"+
								"<td>"+ jsonResult[i].username +"</td>"+
								"<td>"+ jsonResult[i].name +"</td>"+
								"<td>"+ jsonResult[i].age +"</td>"+
								"<td>"+ jsonResult[i].email +"</td>";

						rw.innerHTML+=tc;
						list.appendChild(rw);
						
    			}
    	document.getElementById('userlist').innerHTML="";
		console.log("list added "+ list);
		document.getElementById('userlist').append(list);
		createButton(list);
     	
     }
     
 });
	
}



// handle the event that age condition button greater or equel
document.querySelectorAll('#condition button')[0].onclick = function() {
		var age = document.querySelector('#condition input').value;
		if(age !=0 && age!=null){
				ageCondition(age,"gt")
			}else{
				alert("please enter a number");
		}
}

// handle the event that age condition button less or equel
document.querySelectorAll('#condition button')[1].onclick = function() {
		var age = document.querySelector('#condition input').value;
			if(age !=0 && age!=null){
				ageCondition(age,"lt")
			}else{
				alert("please enter a number");
			}
		}
// create button sorting buttons
function createButton(list){
		document.getElementById('userlist').innerHTML="";
		console.log("list added "+ list);
		document.getElementById('userlist').append(list);
		var button =document.createElement('button');
		button.innerHTML="calculate age";
		button.setAttribute('onclick',"getAge()")
		document.getElementById('userlist').append(button);
		var sort1 = document.createElement('button');
		sort1.innerHTML='sort by name';
		sort1.setAttribute('onclick',"showlist('name')");
		document.getElementById('userlist').append(sort1);
		var sort2 = document.createElement('button');
		sort2.innerHTML='sort by age';
		sort2.setAttribute('onclick',"showlist('age')");
		document.getElementById('userlist').append(sort2);
}


