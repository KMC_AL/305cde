  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyBhlfiYK-odRmnTBM_FuzjfTQa0J9IRDPI",
    authDomain: "account-4c3f3.firebaseapp.com",
    databaseURL: "https://account-4c3f3.firebaseio.com",
    projectId: "account-4c3f3",
    storageBucket: "",
    messagingSenderId: "738616861946"
  };
  firebase.initializeApp(config);

// this is the function to save user data to firebase
function writeUserData( username, name, email , password ,age) {
  firebase.database().ref('users/' + username).set({
    id : Math.random().toString(36).substr(2, 9),
    username: username,
    name: name,
    email : email,
    password : password,
    age : age
  });
}